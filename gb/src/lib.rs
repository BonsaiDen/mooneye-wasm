// Crates ---------------------------------------------------------------------
#![feature(proc_macro)]
#![feature(use_extern_macros)]
extern crate mooneye_gb;

#[macro_use]
extern crate stdweb;

#[macro_use]
extern crate lazy_static;


// STD Dependencies -----------------------------------------------------------
use std::time::Duration;
use std::sync::Mutex;
use std::os::raw::c_void;
use std::slice;
use std::mem;


// External Dependencies ------------------------------------------------------
use stdweb::js_export;
use stdweb::web::{TypedArray};
use mooneye_gb::config::{Bootrom, Cartridge, HardwareConfig, Model};
use mooneye_gb::emulation::{EmuTime, EmuEvents};
use mooneye_gb::machine::Machine;
use mooneye_gb::GbKey;
use mooneye_gb::gameboy::{BootromData, Color, CPU_SPEED_HZ};


// Statics --------------------------------------------------------------------
static DMG_BIOS: [u8; 256] = [
    // Open Source DMG BootRom from "SameBoy":
    49, 254, 255, 33, 0, 128, 34, 203, 108,
    40, 251, 62, 128, 224, 38, 224, 17,
    62, 243, 224, 18, 224, 37, 62, 119,
    224, 36, 62, 252, 224, 71, 17, 4, 1,
    33, 16, 128, 26, 71, 205, 130, 0,
    205, 130, 0, 19, 123, 238, 52, 32,
    242, 17, 177, 0, 14, 8, 26, 19, 34,
    35, 13, 32, 249, 62, 25, 234, 16,
    153, 33, 47, 153, 14, 12, 61, 40, 8,
    50, 13, 32, 249, 46, 15, 24, 245,
    62, 145, 224, 64, 6, 45, 205, 163,
    0, 62, 131, 205, 170, 0, 6, 5, 205,
    163, 0, 62, 193, 205, 170, 0, 6, 70,
    205, 163, 0, 33, 176, 1, 229, 241,
    33, 77, 1, 1, 19, 0, 17, 216, 0,
    195, 254, 0, 62, 4, 14, 0, 203, 32,
    245, 203, 17, 241, 203, 17, 61, 32,
    245, 121, 34, 35, 34, 35, 201, 229,
    33, 15, 255, 203, 134, 203, 70, 40,
    252, 225, 201, 205, 151, 0, 5, 32,
    250, 201, 224, 19, 62, 135, 224, 20,
    201, 60, 66, 185, 165, 185, 165, 66,
    60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 224,
    80
];

lazy_static! {
    static ref STATE: Mutex<Option<State>> = Mutex::new(None);
}


// Struts ---------------------------------------------------------------------
struct State {
    machine: Machine,
    emu_time: EmuTime,
    palette: [u8; 12],
    last_input: [u8; 8],
    input: [u8; 8]
}

impl State {
    fn new(machine: Machine) -> State {
        State {
            machine,
            emu_time: EmuTime::zero(),
            palette: [
                255, 255, 255,
                192, 192, 192,
                64, 64, 64,
                0, 0, 0
            ],
            last_input: [0; 8],
            input: [0; 8]
        }
    }
}


// WASM Interface -------------------------------------------------------------
#[js_export]
fn load_rom(bootrom: TypedArray<u8>, cartridge: TypedArray<u8>) -> String {

    let bootrom = bootrom.to_vec();

    // Fallback to open source DMG BIOS from SameBoy
    let boot = if bootrom.is_empty() {
        let mut bytes = BootromData::new();
        for (i, v) in DMG_BIOS.iter().enumerate() {
            bytes.0[i] = *v;
        }
        Ok(Bootrom {
          model: Model::Dmg,
          data: Box::new(bytes)
        })

    } else {
        let mut bytes = BootromData::new();
        for (i, v) in bootrom.into_iter().enumerate() {
            bytes.0[i] = v;
        }
        Bootrom::from_data(Box::new(bytes))
    };

    match (
        boot,
        Cartridge::from_data(cartridge.to_vec())
    ) {
        (Ok(bootrom), Ok(cartridge)) => {
            let machine = Machine::new(HardwareConfig {
                model: bootrom.model,
                bootrom: Some(bootrom.data),
                cartridge: cartridge
            });
            let mut s = STATE.lock().unwrap();
            *s = Some(State::new(machine));
            "success".to_string()
        },
        (Err(err), _) => err.to_string(),
        (_, Err(err)) => err.to_string()
    }

}

#[js_export]
fn set_palette(entries: Vec<u8>)  {
    if let Ok(ref mut state) = STATE.lock() {
        if let Some(ref mut state) = state.as_mut() {
            for i in 0..12 {
                state.palette[i] = *entries.get(i).unwrap_or(&0);
            }
        }
    }
}


#[js_export]
fn set_input(entries: Vec<u8>)  {
    if let Ok(ref mut state) = STATE.lock() {
        if let Some(ref mut state) = state.as_mut() {

            // Compare inputs
            for i in 0..8 {

                let value = *entries.get(i).unwrap_or(&0);
                state.input[i] = value;

                if state.last_input[i] != value {
                    let key = map_keyindex(i);
                    if value == 1 {
                        state.machine.key_down(key);

                    } else {
                        state.machine.key_up(key);
                    }
                }
            }

            // Copy input
            state.last_input = state.input;

        }
    }
}

fn map_keyindex(index: usize) -> GbKey {
  match index {
    0 => GbKey::Up,
    1 => GbKey::Right,
    2 => GbKey::Down,
    3 => GbKey::Left,
    4 => GbKey::A,
    5 => GbKey::B,
    6 => GbKey::Select,
    7 => GbKey::Start,
    _ => unreachable!()
  }
}

#[js_export]
fn is_ready() -> bool {
    match STATE.lock() {
        Ok(ref v) => {
            v.is_some()
        },
        Err(_) => false
    }
}

#[no_mangle]
pub extern "C" fn emulate(
    pointer: *mut u8,
    width: usize,
    height: usize,
    delta_ms: usize,
    turbo: usize,

) -> usize {

    if let Ok(ref mut state) = STATE.lock() {

        let delta = Duration::from_millis(delta_ms as u64);
        if let Some(ref mut state) = state.as_mut() {
            let cycles = if turbo == 1 {
                CPU_SPEED_HZ as u64 / 60

            } else {
                ((delta * CPU_SPEED_HZ as u32).as_secs() as u64)
            };

            let machine_cycles = EmuTime::from_machine_cycles(cycles / 4);
            let target_time = state.emu_time + machine_cycles;

            let mut result = 0;
            loop {
                let (events, end_time) = state.machine.emulate(target_time);
                if events.contains(EmuEvents::VSYNC) {

                    // TODO output sound

                    let byte_size = width * height * 4;
                    let ext_buffer = unsafe { slice::from_raw_parts_mut(pointer, byte_size) };

                    let buffer = state.machine.screen_buffer();
                    let mut index = 0;
                    for pixel in &buffer[..] {
                        let colors = match pixel {
                            Color::Off => &state.palette[0..3],
                            Color::Light => &state.palette[3..6],
                            Color::Dark => &state.palette[6..9],
                            Color::On => &state.palette[9..12]
                        };
                        ext_buffer[index + 0] = colors[0];
                        ext_buffer[index + 1] = colors[1];
                        ext_buffer[index + 2] = colors[2];
                        ext_buffer[index + 3] = 255;
                        index += 4;
                    }

                    result = 255;

                }

                if end_time >= target_time {
                    state.emu_time = end_time;
                    break;
                }
            }

            result

        } else {
            1
        }

    } else {
        2
    }

}

#[no_mangle]
pub extern "C" fn alloc(size: usize) -> *mut c_void {
    let mut buf = Vec::with_capacity(size);
    let ptr = buf.as_mut_ptr();
    mem::forget(buf);
    return ptr as *mut c_void;
}

#[no_mangle]
pub extern "C" fn draw(pointer: *mut u8, width: usize, height: usize) {
    let byte_size = width * height * 4;
    let buf = unsafe { slice::from_raw_parts_mut(pointer, byte_size) };
    for i in 0..width * height {
        buf[i] = 255;
    }
}

