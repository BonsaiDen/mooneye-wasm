import gb from "./gb/Cargo.toml";

const keyIndex = {
    'ArrowUp': 0,
    'ArrowRight': 1,
    'ArrowDown': 2,
    'ArrowLeft': 3,
    's': 4,
    'a': 5,
    'Shift': 6,
    'Enter': 7
};

class Emulator {

    constructor(el) {

        // Emulation
        this.width = 160;
        this.height = 144;
        this.bootrom = new Uint8Array([]);
        this.cartridge = null;
        this.lastFrame = 0;

        // DOM
        this.keys = [0, 0, 0, 0, 0, 0, 0, 0];
        this.canvas = el.querySelector('canvas');
        this.ctx = this.canvas.getContext('2d');
        this.canvasPointer = global.gbInstance.exports.alloc(this.width * this.height * 4);
        this.initDOM();
        this.debug('Initialized');

    }

    initDOM() {

        this.canvas.addEventListener('dragover', (e) => {
            e.preventDefault();
            e.dataTransfer.dropEffect = "move"
        });

        this.canvas.addEventListener('drop', (e) => {
            e.preventDefault();
            const reader = new FileReader();
            const rom = e.dataTransfer.files[0].slice();
            reader.onload = (e) => {
                const data = new Uint8Array(e.target.result);
                if (data.length === 256) {
                    this.bootrom = data;
                    this.debug('Bootrom set');

                } else {
                    this.cartridge = data;
                    this.debug('Cartridge set');
                }
                setTimeout(this.run.bind(this));
            };
            reader.readAsArrayBuffer(rom);
        });

        window.addEventListener('blur', (e) => {
            Object.values(keyIndex).forEach((i) => this.keys[i] = 0);
        });

        document.body.addEventListener('keydown', (e) => {
            const index = keyIndex[e.key];
            if (index !== undefined) {
                e.preventDefault();
                this.keys[index] = 1;
            }
        });

        document.body.addEventListener('keyup', (e) => {
            const index = keyIndex[e.key];
            if (index !== undefined) {
                e.preventDefault();
                this.keys[index] = 0;
            }
        });

    }

    run() {
        if (this.cartridge) {

            const result = gb.load_rom(
                this.bootrom,
                this.cartridge
            );

            this.debug(`Run: ${result}`);

            gb.set_palette([
                224, 248, 208,
                136, 192, 112,
                52, 104, 86,
                8, 24, 22
            ]);

            this.lastFrame = Date.now();
            window.requestAnimationFrame(this.emulate.bind(this));

        }
    }

    emulate() {

        const now = Date.now();
        const delta = Math.min(now - this.lastFrame, 2000);
        this.lastFrame = now

        gb.set_input(this.keys);

        if (gbInstance.exports.emulate(this.canvasPointer, this.width, this.height, delta, 0) === 255) {

            const data = new Uint8ClampedArray(
                global.gbInstance.exports.memory.buffer,
                this.canvasPointer,
                this.width * this.height * 4
            );

            const bufferImage = new ImageData(data, this.width, this.height);
            this.ctx.putImageData(bufferImage, 0, 0);
        }

        window.requestAnimationFrame(this.emulate.bind(this));

    }

    debug() {
        console.log.apply(console, arguments)
    }

}

global.Emulator = new Emulator(document.querySelector('.gameboy'));

