const original = WebAssembly.instantiate;

WebAssembly.instantiate = function(a, b) {
    return original.call(WebAssembly, a, b).then((instance) => {
        global.gbInstance = instance;
        return instance;
    });
}

