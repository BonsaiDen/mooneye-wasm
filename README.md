# WASM Mooneye GB

Demo: https://bonsaiden.gitlab.io/mooneye-wasm

Web-Enabled Version of Geekio's Rust based [Mooneye GB](https://github.com/Gekkio/mooneye-gb) Emulator.

Ported via [cargo-web](https://github.com/koute/cargo-web) and [stdweb](https://github.com/koute/stdweb).

## Compilation

```
git clone git@github.com:Gekkio/mooneye-gb.git ../mooneye-gb 
rustup update nightly
rustup override add nightly
cargo install --force cargo-web
npm install
parcel index.html
```

## Pages Build

```
parcel build index.html --public-url https://bonsaiden.gitlab.io/mooneye-wasm/
```

## License

- GlueCode licensed under MIT.

- Mooneye GB is licensed under GPLv3+. Copyright (C) 2014-2018 Joonas Javanainen joonas.javanainen@gmail.com

- SameBoy Open Source BIOS licensed under MIT.

